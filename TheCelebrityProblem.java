//{ Driver Code Starts
//Initial Template for Java

import java.io.*;
import java.util.*; 

class GFG{
    public static void main(String args[]) throws IOException { 
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t>0)
        {
            int N = sc.nextInt();
            int M[][] = new int[N][N];
            for(int i=0; i<N; i++)
            {
                for(int j=0; j<N; j++)
                {
                    M[i][j] = sc.nextInt();
                }
            }
            System.out.println(new Solution().celebrity(M,N));
            t--;
        }
    } 
} 
// } Driver Code Ends


class Solution
{ 
    // Function to find if there is a celebrity in the party or not.
    int celebrity(int M[][], int n)
    {
        // Initialize two pointers for the leftmost and rightmost persons.
        int left = 0;
        int right = n - 1;
        
        // Keep moving pointers until they cross each other.
        while (left < right) {
            // If left knows right, left cannot be a celebrity, move right.
            if (M[left][right] == 1)
                left++;
            // Otherwise, right cannot be a celebrity, move left.
            else
                right--;
        }
        
        // Now, left may potentially be a celebrity, but we need to verify.
        // A celebrity is known to everyone, but knows no one.
        for (int i = 0; i < n; i++) {
            // If left knows anyone, or if anyone doesn't know left, left cannot be a celebrity.
            if (i != left && (M[left][i] == 1 || M[i][left] == 0))
                return -1;
        }
        
        // If we reach here, left is a potential celebrity.
        return left;
    }
}
